====
dBnx
====


This package is a simple wrapper around numpy, scipy and uncertainties, which
tries to simplify some common tasks.


Description
===========

This project aims to simplify some common tasks, like

- read/extract data and e.g. simply split datasets from oscilloscopes
- preprocess said data: smoothing, find peaks, ...
- fit data and extract parameters with their uncertainty
- plot data with errorbars

The aim is to quickly say something about an experiment and the information, which
can be extracted.

.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.0.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
