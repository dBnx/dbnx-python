def linear( x, k, d ):
    return k*x + d


def lorentz( x, x_0, Gamma ):
    from scipy.constants import pi
    Gamma_2 = Gamma / 2.0
    return 1 / pi * Gamma_2 / ( ( x - x_0 )**2 + Gamma_2**2 )
