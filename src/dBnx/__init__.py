import sys

if sys.version_info[ :2 ] >= ( 3, 8 ):
    # TODO: Import directly (no need for conditional) when `python_requires = >= 3.8`
    from importlib.metadata import PackageNotFoundError, version # pragma: no cover
else:
    from importlib_metadata import PackageNotFoundError, version # pragma: no cover

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = "dBnx"
    __version__ = version( dist_name )
except PackageNotFoundError: # pragma: no cover
    __version__ = "unknown"
finally:
    del version, PackageNotFoundError

__author__ = "Dave"
__copyright__ = "Dave"

from .core import setup_global_config, foo, umean, fit
import dBnx.func
