import scipy
import numpy as np
import pandas as pd
import scipy.constants as c
import matplotlib.pyplot as plt

import uncertainties.unumpy as unp
from uncertainties import ufloat
from uncertainties import ufloat_fromstr
from uncertainties.unumpy import nominal_values as nom
from uncertainties.unumpy import std_devs as dev


def setup_global_config():
    """ Sets common global options of packages, wich only alter visuals and does otherwise not affect the program.

    Sets numpy precision to 2 an enables LaTex in matplotlib and sets the 
    font family to sefif
    """
    np.set_printoptions( precision=2 )
    plt.rcParams.update( {
        "text.usetex": True,
        "font.family": "serif",
    } )


def foo( b: bool = True ) -> int:
    """ Simple function for testing

    :function: TODO
    :returns: TODO

    """
    return 42 if b else -1


def umean( array ):
    """ Calculates the uncertainty weighted mean of the input

    Parameters
    ----------
    array : np.array
        Numpy array of uncertainty ufloats

    Returns
    -------
    uncertainty.ufloat
        Uncertainty weighted mean of the input
    """
    w = 1.0 / dev( array )**2
    wsum = np.sum( w )
    mean_unc = np.sqrt( 1.0 / wsum )
    return ufloat( np.mean( unp.nominal_values( array ) ), mean_unc )


def fit( func, x, y, init=None, bounds=None, sigma=None ):
    popt, pcov = scipy.optimize.curve_fit(func,
                           xdata=x,
                           ydata=y,
                           p0=init,
                           bounds=bounds,
                           sigma=sigma, absolute_sigma=True,
                           maxfev=140000) if init is not None and bounds is not None else \
        scipy.optimize.curve_fit(func,
                  xdata=x,
                  ydata=y,
                  p0=init,
                  sigma=sigma, absolute_sigma=True,
                  maxfev=140000) if bounds is None else\
        scipy.optimize.curve_fit(func,
                  xdata=x,
                  ydata=y,
                  sigma=sigma, absolute_sigma=True,
                  maxfev=140000)  # if init is None
    perr = np.sqrt( np.diag( pcov ) )
    return popt, *[
        ufloat( popt[ i ], perr[ i ] ) for i in range( len( popt ) )
    ]
