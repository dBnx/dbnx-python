

.PHONY: clean
clean:
	tox -e clean
	rm -rf src/dBnx.egg-info

.PHONY: build
build: 
	tox -e build

.PHONY: clean build upload
upload: build 
	tox -e publish -- --repository testpypi

.PHONY: install_as_symlink
install_as_symlink: 
	mv pyproject.toml pyproject.tmp
	pip install -e .  # etc
	mv pyproject.tmp pyproject.toml

.PHONY: install_offline
install_offline: build
	python -m pip install ./dist/dBnx-?.?.?-py3-none-any.whl --force-reinstall

.PHONY: install_remote
install_remote: 
	python -m pip install --index-url https://test.pypi.org/simple/ dBnx

.PHONY: test
test: 
	#python setup.py pytest
	tox
